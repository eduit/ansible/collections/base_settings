Role Name
=========

Set the state of SELinux on RHEL7 and RHEL8.

Requirements
------------

None.

Role Variables
--------------

- `selinux_disable`: defaults to `true`

Dependencies
------------

None.

