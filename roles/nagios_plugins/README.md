nagios_plugins
=========

Install nagios, standard plugins and custom plugins for LET. Tested on RHEL8 and Debian10.

Requirements
------------

Access to the nagios user is granted by a SSH public key. This key is deployed by the *authentication* role in this collection.

Example Playbook
----------------

Simple, because no parameters are involved:

    - hosts: all
      roles:
         - let.base_settings.nagios_plugins
