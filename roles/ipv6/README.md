Role Name
=========

Manage the availability of IPv6.

Requirements
------------

None.

Role Variables
--------------

- `ipv6_disable`: defaults to `true`

Dependencies
------------

None.

