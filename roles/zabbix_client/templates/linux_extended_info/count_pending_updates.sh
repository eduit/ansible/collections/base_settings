#!/usr/bin/bash
#
# Helper script for Zabbix template "Linux extended info"

. /etc/os-release

if [ "$ID" == "debian" ] || [ "$ID" == "ubuntu" ]; then
    apt-get -q -y --ignore-hold --allow-change-held-packages --allow-unauthenticated -s dist-upgrade | /bin/grep  ^Inst | wc -l
elif [ "$ID_LIKE" == "fedora" ]; then
    dnf check-update -q|grep -c ^[a-z0-9]
fi
