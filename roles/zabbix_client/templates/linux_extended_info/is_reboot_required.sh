#!/usr/bin/bash
#
# Helper script for Zabbix template "Linux extended info"

. /etc/os-release

if [ "$ID" == "debian" ] || [ "$ID_LIKE" == "debian" ]; then
    if [ -f /var/run/reboot-required ]; then
	echo 1
    else
	echo 0
    fi
fi

if [ "$ID_LIKE" == "fedora" ]; then
	if [ "$(needs-restarting -r | grep >/dev/null 'Reboot is required')" == "0" ]; then
	echo 1
    else
	echo 0
    fi
fi

    
