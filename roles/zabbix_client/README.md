# Zabbix Agent Deployment

Installs the Zabbix agent and set registration metadata.

# Basic Usage
For configuration options see `defaults/main.yml`

Agent installation requires two steps:

## Install Agent Package

```yaml
- name: Zabbix client installation
  hosts: zabbix_agent
  user: root
  roles:
    - let.base_settings.zabbix_client
```

Installs the package and defines the variable with information for the registration process: *zabbix_client_host_metadata*. 
The Zabbix server will configure additional templates according to the metadata information. 

Setup fully integrated in Ansible like PostgeSQL, MariaDB, Redis etc. will extend the metadata string during the service setup.

## Launch and Registration
With the metadata string configured, the finalization tasks will configure the agent, launch it and perform the registration:

```yaml
# Call finalization after all service roles have been completed
- name: Register Zabbix agent
  hosts: zabbix_agent,Production
  tasks:
    - name: Finalize Zabbix registration
      ansible.builtin.include_role:
        name: let.base_settings.zabbix_client
        tasks_from: finalize.yml
```

For hosts not fully integrated, the metadata string can be extended manually before the finalization:

```yaml
- name: Register Zabbix agent
  hosts: zabbix_agent,Production
  tasks:
    - name: Extend registration string for CodeExpert
      ansible.builtin.set_fact:
        zabbix_client_host_metadata: "{{ zabbix_client_host_metadata }} codeexpert"
      when: inventory_hostname_short in groups.CodeExpert

    - name: Finalize Zabbix registration
      ansible.builtin.include_role:
        name: let.base_settings.zabbix_client
        tasks_from: finalize.yml
```

## User Parameters
Can be defined with
- `zabbix_client_userparameters_global` for all systems
- `zabbix_client_userparameters` additionally, optional

Script templates have to be in `templates/userparameters` and are deployed with *ansible.builtin.template*. They will be called by cronjobs, because on some OS (RHEL), information about packages is restricted to root.

Definition:

```yaml
zabbix_client_userparameters_global:
  - script: count_pending_updates.sh
    result: /tmp/pending_updates
    cron: "30 * * * *"
  - script: is_reboot_required.sh
	result: /tmp/reboot_required
    cron: "*/5 * * * *"
```
