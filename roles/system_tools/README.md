System Tools
=========

Installs basic system tools.

Requirements
------------

None.

Role Variables
--------------

Package lists are defined in `defaults`:

- `system_tools_packages`: packages to be installed
- `system_tools_obsolete_packages`: packages to be removed
- `system_tools_rpm_packages`: packages with names specific to RPM
- `system_tools_deb_packages`: packages with names spefific to DEB

Dependencies
------------

None.
