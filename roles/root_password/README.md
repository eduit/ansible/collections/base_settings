root_password
=========

Set the root password following the ID-EDUIT conventions: secret part 1 + first letter of the hostname + secret part 2.

Role Variables
--------------

- `pwd_part_1`, `pwd_part_2`: parts of the password

The variables are stored in the variables file `password_template.yml`:

```
pwd_part_1: first_part
pwd_part_2: second_part
```

Because the file is encrypted with *ansible_vault*, the variables and the structure are not directly visible.

### Encryption/Decryption
The ansible-vault password can be found in Keepass. See [this article in Confluence](https://unlimited.ethz.ch/display/SYSADMIN/Ansible+Vault) for instructions for encryption and decryption.

Example Playbook
----------------

Simply:

    - hosts: all
      roles:
         - let.base_settings.root_password

