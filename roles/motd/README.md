Role Name
=========

Set information displayed during login (like motd, but scripted)

Requirements
------------

None.

Role Variables
--------------

No configurable variables, all information used is pulled from SnipeIT during the configuration run.

Dependencies
------------

None.
