Tivoli Backup Client
===================

Executed if system is in group _backup_.

Variables
---------

- `tsm_nodename`: defaults to `inventory_hostname_short`
- `tsm_tcpserveraddress`: "" (hostname of backup server)
- `tsm_tcpport`: ""
- `tsm_password`: ""; set this to the standard password in `groupvars/all.yml`

Inclusion/Exclusion
-------------------

Lists of paths to in- or exclude:

- `tsm_include_dir`: []
- `tsm_exclude_dir`: []

Lists of files names to in- or exclude. To i.e. exclude a filename everywhere in thefilesystem write `/.../core`:

- `tsm_include_file`: []
- `tsm_exclude_file`: []
