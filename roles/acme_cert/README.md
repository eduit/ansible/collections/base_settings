acme_cert
=========

Obtain TLS certificates using the ACME protocol.

Requirements
------------

Webserver installed and port 80 reachable for the challenger. The local firewalld service can be opened with an option, see below.

Role Variables
--------------

Mandatory:
- `acme_domains`: list of DNS names to be included in the certificate (base name and SANs)

The play will be skipped if no `acme_domains` are set.

Optional:
- `acme_email`: used for registration and (with Let's Encrypt) reminder mails
- `acme_web_root`: where to place the challenge files
- `acme_open_webports`: opens ports 80 and 443 in firewalld; default true
- `acme_challenge_type`: default `http-01`, others untested
- `acme_directory`: URI to contact for certificate issuing. Default: LE staging URI
- `acme_version`: defaults to 2
- `acme_cert_directory`: where to store keys/certs (`/etc/acme_cert`)

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - role: let.base_settings.acme_cert
      vars:
        acme_directory: https://acme-v02.api.letsencrypt.org/directory
        acme_domains:
	  - mysrv.ethz.ch
          - www.mysrv.ethz.ch
	  - admin.mysrv.ethz.ch


Result
------

Generates the files in the directory `/etc/letsencrypt`:

- `account/account.key`: certificate used for authentication for subsequent renewals
- `keys/acme_cert.key`: private key for server certificate
- `csrs/acme_cert.csr`: certificate request
- `certs/acme_cert.crt`: web server certificate
- `certs/chain_acme_cert.crt`: root and intermediate certificate(s)
- `certs/fullchain_acme_cert.crt`: all certificates combined: root, intermediate and server certificate
