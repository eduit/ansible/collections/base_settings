Role Name
=========

Firewalld configuration for EduIT environment.

Role Variables
--------------

- `firewall_zone`: zone used as default, set to `drop`

Firewall Sets
-------------

Currently (7. Jun. 2024), the following sets are provided for:

- Moodle
- Elasticsearch
- Kibana
- Groundwork
- PolyBook
- OP-Portal
- SEB-Server (DB, no vars file needed)

Each set is configured in a separate file in `vars/main/`.

Custom Services
------------

`files/services` contains custom service definitions. The distribution is controlled by `tasks/main/services.yml`.
